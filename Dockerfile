FROM mambaorg/micromamba:1.2.0
ENV ATOLLGEN_FOLDER=/data
COPY --chown=$MAMBA_USER:$MAMBA_USER ./environment.yml /app/environment.yml
WORKDIR /app
RUN micromamba install -y -n base -f environment.yml && micromamba clean --all --yes
COPY --chown=$MAMBA_USER:$MAMBA_USER . /app
ARG MAMBA_DOCKERFILE_ACTIVATE=1
RUN pip install .
USER root
RUN mkdir -p $ATOLLGEN_FOLDER
RUN chown -R $MAMBA_USER:$MAMBA_USER $ATOLLGEN_FOLDER
USER $MAMBA_USER

ENTRYPOINT ["/usr/local/bin/_entrypoint.sh", "atollgen-cli"]