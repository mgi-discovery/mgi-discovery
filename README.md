# AtollGen-CLI

Library for AtollGen GIs annotation and classification.
Annotate and classify GI sequences from a simple accession number
and GI coordinates in host genome.

AtollGen CLI is a part of the [AtollGen Project](https://atollgen.gitlab.io/docs/)

Full API Documentation is available at https://atollgen.gitlab.io/atollgen-cli

## Installation

For now, the only way to install atollgen is to clone the repository and install
it manually. It comes with a environment.yml file that can be used to create a
conda environment with all the dependencies.

The tool target Linux systems.

```bash
git clone git+https://gitlab.com/atollgen/atollgen-cli.git
cd atollgen-cli
conda env create -f environment.yml
conda activate atollgen-cli
pip install .
```

### Docker

If docker is installed, you can avoid all the dependencies installation with:

```bash
docker run -v atollgen-data:/data registry.gitlab.com/atollgen/atollgen-cli:latest <cmd>
```

## Command-Line Interface

The CLI allows to run the full pipeline in a terminal. The usage is as follows :

First, run the preprocess command that will download all the needed files to run
the pipeline with :

`atollgen-cli preprocess`

The genomes are fetched from the NCBI and stored to avoid multiple requests.

### For one GI sequence

Then, run the pipeline for one GI sequence with :

`atollgen-cli run <acc>:<start>..<end>`

IE:

```bash
IE: atollgen-cli run NZ_SMZE01000022:7039..37242
```

### For multi GI sequences

Multi-GI analysis is possible using :

`atollgen-cli many <island_db> <output_folder>`

<island_db> being a csv file that contains the following columns :
"accession","start","end", and as many lines as GIs you want to study.
All the results will be stored as a json file in <output_folder>
(which will be created if it does not exist prior).

If a GI has already been analyzed and has its own json output file,
it won't be analyzed again. If you want to run the analysis again
and overwrite the results, use the following command :

`atollgen-cli many --overwrite <island_db> <output_folder>`


## As a python library

The user API is built on two main components : `atollgen-cli.Island` and the
different `atollgen-cli.Annotator`. The Island object contain the main
information (at least the accession id, start and end coordinate of the island),
and can hold the different annotations added by the different `annotators`.

```python
import json
from pathlib import Path

from entrez_fetcher import GenomeStore

island_info = {"accession": "NZ_SMZE01000022", "start": 7039, "end": 37242}
genome_store = GenomeStore()
categ_mapping = json.loads(Path("./data/inputs/categs.json").read_text())
```

We then initialize the annotators : the annotation come from
[HMMER](http://hmmer.org/documentation.html), defense-finder,
[RGI](https://github.com/arpcard/rgi) and
[tRNAscan-SE](https://github.com/UCSC-LoweLab/tRNAscan-SE).

The annotators take care of which part of the sequence have to be analyzed and
what kind of sequence (nucl. or amino-acid.). The sequences are split thanks to
the `atollgen-cli.SequenceHandler` that will take the host genome sequence,
extract the island part and its environment and give access to the different
areas. It uses [Prodigal] to detect and extract the orfs, and give also access
to the different genes of the different areas. These orfs are automatically
added as CDS to the island at instantiation.

The main areas are

- left_env
- left_ext
- bulk
- right_ext
- right_env

left_env, left_ext, bulk, right_ext, right_env

And the compound areas are

- all (left_env, left_ext, bulk, right_ext, right_env)
- island (left_ext, bulk, right_ext)
- envs (left_env, right_env)
- exts (left_ext, right_ext)

```python
from atollgen_cli import annotators

hmm_int_annot = annotators.HmmerAnnotator(
    "./data/inputs/hmm/int.hmm",
    origin="integrase",
    area="exts",
    pfam_mapping=categ_mapping,
)
hmm_mob_annot = annotators.HmmerAnnotator(
    "./data/inputs/hmm/mob.hmm",
    origin="mobility",
    area="island",
    pfam_mapping=categ_mapping,
)
hmm_pfam_annot = annotators.HmmerAnnotator(
    "./data/inputs/hmm/Pfam-A.hmm",
    origin="pfam-A",
    area="envs",
    pfam_mapping=categ_mapping,
)
dfinder_annot = annotators.DFinderAnnotator(
    "./data/interim/macsyfinder", origin="defensefinder"
)
rgi_annot = annotators.RGIAnnotator(origin="rgi")
trna_annot = annotators.TrnaScanAnnotator(origin="trnascan")
```

All these annotators has a `annotator.annotate(island)` method that will do the
analysis and add annotations to the Island object.

```python
from atollgen_cli import Island

>>> island = Island(**island_info, genome_store=genome_store)
>>> island.annotate(hmm_int_annot)
>>> island.annotate(hmm_mob_annot)
>>> island.annotate(hmm_pfam_annot)
>>> island.annotate(dfinder_annot)
>>> island.annotate(rgi_annot)
>>> island.annotate(trna_annot)
>>> {
...     key: [annot.origin for annot in cds.annotations]
...     for key, cds in island.CDSs.items()
...     if cds.annotations
... }
{(5729, 5941): ['pfam-A',
  'pfam-A',
  'pfam-A',
  'pfam-A',
  'pfam-A',
  'pfam-A',
  'pfam-A',
  'pfam-A',
  'pfam-A'],
 (6101, 7147): ['pfam-A', 'pfam-A'],
 (7243, 8343): ['integrase'],
 (18932, 21688): ['mobility'],
 (24658, 28044): ['mobility', 'mobility'],
 (31267, 32232): ['mobility'],
 (37625, 38089): ['pfam-A',
  'pfam-A',
  'pfam-A',
  'pfam-A',
  'pfam-A',
  'pfam-A',
  'pfam-A'],
 (38099, 38560): ['pfam-A', 'pfam-A'],
 (38677, 39165): ['pfam-A']}
```

The island object can then be saved as json.

```python
>>> island = Island(**island_info, genome_store=genome_store)
>>> island.annotate(hmm_int_annot)
>>> print(island.to_json(indent=4, keep_empty_cds=False))
{
    "accession": "NZ_SMZE01000022",
    "start": 7039,
    "end": 37242,
    "host_size": 49006,
    "version": null,
    "label": null,
    "extra": {},
    "CDSs": {
        "7243:8343": {
            "start": 7243,
            "end": 8343,
            "strand": "+",
            "area": "left_ext",
            "bound_distance": -1304,
            "annotations": [
                {
                    "origin": "integrase",
                    "quals": {
                        "query_len": 172,
                        "relquery_start": 15,
                        "relquery_end": 170,
                        "coverage_len": 155,
                        "coverage_factor": 0.9069767441860465,
                        "bitscore": 60.8,
                        "evalue": 7.5e-21,
                        "domain_reltarget_start": 198,
                        "domain_reltarget_end": 344,
                        "domain_size": 146,
                        "domain_cds_coverage": 0.13272727272727272,
                        "categs": "INT_tyr",
                        "area": "left_ext"
                    }
                }
            ]
        }
    },
    "coords": {
        "left_env": 5039,
        "start_island": 7039,
        "left_ext": 12039,
        "right_ext": 32242,
        "end_island": 37242,
        "right_env": 39242
    }
}
```
