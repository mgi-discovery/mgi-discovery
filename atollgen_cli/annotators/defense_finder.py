from __future__ import annotations

from dataclasses import dataclass
from io import StringIO
from typing import Literal
from loguru import logger

import pandas as pd
from pydantic import DirectoryPath

from ..cmds import defensefinder
from .base import Annotator, Area, Island, add_annot_from_df


@dataclass
class DFinderAnnotator(Annotator):
    """Annotator for the DefenseFinder tool. DefenseFinder is a tool for the
    identification of antimicrobial resistance genes in bacterial genomes.

    It will work on the faa file of the all island sequence (but not the island
    environment).

    Parameters
    ----------
    model_dir : DirectoryPath
        The path to the directory containing the models for DefenseFinder.

    Notes
    -----

    The DefenseFinder output is a tab-separated file with the following columns:

    - hit_id: the id of the hit
    - gene_name: the name of the gene
    - model_fqn: the full name of the model
    - hit_i_eval: the i_eval of the hit
    - hit_score: the score of the hit
    - hit_seq_cov: the sequence coverage of the hit

    """

    model_dir: DirectoryPath
    area: Area = "island"
    origin: str = "defense_finder"
    on = "faa"

    _cols = [
        "hit_id",
        "gene_name",
        "model_fqn",
        "hit_i_eval",
        "hit_score",
        "hit_seq_cov",
        "type",
        "subtype",
    ]

    def run(self, island: Island) -> str:
        """Run the annotator on the island. It will run DefenseFinder on the faa file.

        Parameters
        ----------
        island : Island
            The island to annotate.

        Returns
        -------
        str
            DefenseFinder output (a tsv) as a string.
        """
        logger.info(
            f"Running DefenseFinder on {island.id} (origin: {self.origin}, area: {self.area})"
        )
        with self.path(island) as input_faa:
            if input_faa.stat().st_size == 0:
                raise FileNotFoundError(f"File {input_faa} is empty.")
            output = input_faa.parent / "dfinder"
            defensefinder(input_faa, output, model_dir=self.model_dir)
            return (output / "all_best_solutions.tsv").read_text()

    def process(self, island: Island) -> pd.DataFrame:
        """Process the output of DefenseFinder and return a dataframe with the
        annotations. The columns of the dataframe are the same as the quals of the
        annotation.

        Parameters
        ----------
        island : Island
            The island to annotate.

        Returns
        -------
        DataFrame
            The dataframe containing the annotations. The columns are the same as
            the quals of the annotation. If no annotation is found, an empty
            dataframe is returned.
        """
        logger.info(
            f"Processing DefenseFinder output on {island.id} (origin: {self.origin}, area: {self.area})"
        )
        try:
            dfinder_content = self.run(island)
            df = pd.read_csv(StringIO(dfinder_content), sep="\t", comment="#")
            df = df[
                [
                    "hit_id",
                    "gene_name",
                    "model_fqn",
                    "hit_i_eval",
                    "hit_score",
                    "hit_seq_cov",
                ]
            ]
        except (pd.errors.EmptyDataError, FileNotFoundError):
            return pd.DataFrame([], columns=self._cols)
        df[["type", "subtype"]] = df.model_fqn.str.rsplit("/", n=2, expand=True).drop(
            0, axis=1
        )
        df = df.drop("model_fqn", axis=1)
        return df

    def annotate(self, island: Island) -> Island:
        """Run DefenseFinder on the faa file, process the output and add the
        annotations to the island. The Island will be modified in place.

        Parameters
        ----------
        island : Island
            The island to annotate.

        Returns
        -------
        Island
            The annotated island.
        """

        df = self.process(island)
        add_annot_from_df(island, df, origin=self.origin)
        return island

    def check(self):
        """Check if the model directory exists and contains the defense-finder-models
        subdirectory.
        """
        if not (self.model_dir / "defense-finder-models" / "metadata.yml").exists():
            raise FileNotFoundError(
                f"Model directory {self.model_dir} does not contain the "
                "defense-finder-models subdirectory."
            )
