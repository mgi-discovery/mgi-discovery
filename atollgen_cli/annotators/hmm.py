from __future__ import annotations


from dataclasses import dataclass, field
from io import StringIO
from pathlib import Path
from typing import Dict

import numpy as np
import pandas as pd
import plumbum
from Bio import SearchIO
from loguru import logger

from ..cmds import hmmsearch
from .base import Annotator, Area, Island, add_annot_from_df


def _parse_hit(hit):
    return {
        "id": hit.id,
        "evalue": hit.evalue,
        "bitscore": hit.bitscore,
        "desc": hit.description,
    }


def _parse_hsp(hsp, qlen):
    query_start, query_end = hsp.query_range
    return {
        "query_len": qlen,
        "relquery_start": query_start,
        "relquery_end": query_end,
        "relquery_size": abs(query_end - query_start),
        "query_coverage": abs(query_end - query_start) / qlen,
        "domain_reltarget_start": hsp.hit_start,
        "domain_reltarget_end": hsp.hit_end,
        "domain_size": abs(hsp.hit_start - hsp.hit_end),
        "domain_cvalue": hsp.evalue_cond,
        "domain_ivalue": hsp.evalue,
        "domain_bitscore": hsp.bitscore,
    }


def _parse_query_domain(query):
    df_hits = pd.DataFrame((_parse_hit(hit) for hit in query.hits)).set_index("id")
    df_hsps = pd.concat(
        [
            pd.DataFrame((_parse_hsp(hsp, query.seq_len) for hsp in hit.hsps))
            for hit in query.hits
        ],
        keys=df_hits.index,
    ).rename_axis(["hit_id", "hsp_id"])
    df = df_hsps.join(df_hits.add_prefix("hit_"), on="hit_id")
    return df


def parse_domain_file(domain_file: Path) -> pd.DataFrame:
    """Parse a domain file from hmmsearch.

    Parameters
    ----------
    domain_file : Path
        The path to the domain file.

    Returns
    -------
    pd.DataFrame
        A dataframe with the parsed domain file.
    """
    try:
        queries = list(SearchIO.parse(domain_file, "hmmsearch3-domtab"))
        df = pd.concat(
            [_parse_query_domain(query) for query in queries],
            keys=[
                acc.split(".")[0] if (acc := query.accession) != "-" else query.id
                for query in queries
            ],
            names=["query_id"],
        )
    except ValueError as e:
        df = pd.DataFrame()
    return df


def _get_best_domain(df: pd.DataFrame) -> pd.DataFrame:

    best_domain = df.iloc[df.domain_bitscore.argmax()]
    return pd.DataFrame(
        [
            {
                "query_len": best_domain.query_len,
                "relquery_start": best_domain.qrange[0],
                "relquery_end": best_domain.qrange[1],
                "coverage_len": np.ptp(best_domain.qrange),
                "coverage_factor": (np.ptp(best_domain.qrange) + 1)
                / best_domain.query_len,
                "bitscore": best_domain.domain_bitscore,
                "evalue": best_domain.domain_cvalue,
                "domain_reltarget_start": best_domain.domain_reltarget_start,
                "domain_reltarget_end": best_domain.domain_reltarget_end,
                "domain_size": best_domain.domain_size,
            }
        ],
    )


def get_best_domain_coverage(df_dom: pd.DataFrame) -> pd.DataFrame:
    """Get the best domain for each query.

    Parameters
    ----------
    df_dom : pd.DataFrame
        The dataframe with the parsed domain file.

    Returns
    -------
    pd.DataFrame
        A dataframe with the best domain for each query.
    """

    qrange = (
        df_dom[["relquery_start", "relquery_end"]].apply(tuple, axis=1).rename("qrange")
    )
    return df_dom.join(qrange).groupby(["query_id", "hit_id"]).apply(_get_best_domain)


@dataclass(frozen=True)
class HmmerAnnotator(Annotator):
    """Annotate an island with HMMER. HMMER is used to annotate the island with
    the given HMM file. Work on faa files.

    Parameters
    ----------
    hmm_file : Path
        The path to the HMM file.

    pfam_mapping : Dict[str, str], optional
        A dictionary mapping the HMMER hit_id to the pfam_id, by default None.

    area : Area, optional
        The area to annotate, by default "island".

    origin : str, optional
        The origin of the annotation, by default "hmmer".

    Notes
    -----
    The HMMER output is parsed with Biopython. The best domain for each query is
    selected. The best domain is the one with the highest bitscore.

    The output is a dataframe with the following columns:

    - query_id: the query id
    - hit_id: the hit id
    - query_len: the query length
    - relquery_start: the start of the query relative to the query length
    - relquery_end: the end of the query relative to the query length
    - coverage_len: the length of the coverage
    - coverage_factor: the coverage factor
    - bitscore: the bitscore
    - evalue: the evalue
    - domain_reltarget_start: the start of the domain relative to the target
      length
    - domain_reltarget_end: the end of the domain relative to the target length
    - domain_size: the size of the domain
    - domain_cds_coverage: the coverage of the domain relative to the CDS
      length
    - area: the area of the annotation
    """

    hmm_file: Path
    pfam_mapping: Dict[str, str] = field(default_factory=dict, hash=False)
    area: Area = "island"
    origin: str = "hmmer"
    on = "faa"

    _cols = [
        "query_id",
        "hit_id",
        "query_len",
        "relquery_start",
        "relquery_end",
        "coverage_len",
        "coverage_factor",
        "bitscore",
        "evalue",
        "domain_reltarget_start",
        "domain_reltarget_end",
        "domain_size",
        "domain_cds_coverage",
        "area",
    ]

    def run(self, island: Island) -> str:
        """Run HMMER on the island. Output the domain file content as string.

        Parameters
        ----------
        island : Island
            The island to annotate.

        Returns
        -------
        str
            The content of the domain file.
        """

        logger.info(
            f"Running HMMER on {island.id} (origin: {self.origin}, area {self.area})"
        )
        with self.path(island) as input_faa:
            blout = input_faa.with_suffix(".blout")
            hmmsearch(self.hmm_file, input_faa, blout)
            return blout.read_text()

    def process(self, island: Island) -> pd.DataFrame:
        """Run hmmer, process the domain file content and return a dataframe with the
        annotation.

        Parameters
        ----------
        island : Island
            The island to annotate.

        Returns
        -------
        pd.DataFrame
            A dataframe with the annotation.
        """
        logger.info(
            f"Processing HMMER on {island.id} (origin: {self.origin}, area {self.area})"
        )
        domain_content = self.run(island)
        cds_info = pd.DataFrame(island.seq_handler.cds_info).set_index("cds_id")
        df_domain = parse_domain_file(StringIO(domain_content))
        if not len(df_domain):
            return pd.DataFrame([], columns=self._cols)
        best_domain = get_best_domain_coverage(df_domain).droplevel(2)
        best_domain["domain_cds_coverage"] = (
            best_domain.domain_size / best_domain.join(cds_info, on="hit_id").cds_size
        )
        best_domain["categs"] = best_domain.index.get_level_values("query_id").map(
            self.pfam_mapping,
        )
        best_domain["categs"] = best_domain["categs"].apply(
            lambda categs: ";".join(categs) if categs == categs else None
        )
        best_domain = best_domain.join(cds_info.area, on="hit_id")
        return best_domain.reset_index()

    def annotate(self, island: Island) -> Island:
        """Run HMMER on the island and add the annotation to the island. The island is
        modified in place.

        Parameters
        ----------
        island : Island
            The island to annotate.

        Returns
        -------
        Island
            The annotated island.
        """

        try:
            df = self.process(island)
            add_annot_from_df(island, df, origin=self.origin)
        except plumbum.ProcessExecutionError as e:
            logger.warning(e)
        return island

    def check(self):
        """Check that the HMM file exists."""

        if not self.hmm_file.exists():
            raise FileNotFoundError(f"The HMM file {self.hmm_file} does not exist.")
