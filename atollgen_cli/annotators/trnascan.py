from __future__ import annotations

import re
from dataclasses import dataclass
from io import StringIO
from loguru import logger

import pandas as pd

from ..cmds import trnascan
from .base import Annotator, Area, Island

_TRNA_NAME_PATTERN = re.compile(
    r"^(?P<cds_name>(?P<chr>\S+):(?P<fna_start>\d+)..(?P<fna_end>\d+))"
)


@dataclass
class TrnaScanAnnotator(Annotator):
    """Annotate tRNAs using trnascan. It will work on the fna file of the island
    environment.

    Parameters
    ----------
    area : Area
        The area to annotate.
    origin : str
        The origin of the annotation.

    Notes
    -----
    The trnascan output is a tab-separated file with the following columns:
    - cds_name
    - trna_index
    - annot_start
    - annot_end
    - trna_type
    - anti_codon
    - intron_start
    - intron_end
    - inf_score
    - isotype_cm
    - isotype_score
    - note
    """

    area: Area = "envs"
    origin: str = "trnascan"
    on = "fna"

    _cols = [
        "cds_name",
        "trna_index",
        "annot_start",
        "annot_end",
        "trna_type",
        "anti_codon",
        "intron_start",
        "intron_end",
        "inf_score",
        "isotype_cm",
        "isotype_score",
        "note",
    ]

    def run(self, island: Island) -> str:
        """Run the annotator on the island. It will run trnascan on the fna file.

        Parameters
        ----------
        island : Island
            The island to annotate.

        Returns
        -------
        str
            The trnascan output file content as string.

        Notes
        -----
        The trnascan output is a tab-separated file with the following columns:

        - cds_name
        - trna_index
        - annot_start
        - annot_end
        - trna_type
        - anti_codon
        - intron_start
        - intron_end
        - inf_score
        - isotype_cm
        - isotype_score
        - note

        """
        logger.info(
            f"Running trnascan on {island.id} (origin: {self.origin}, area: {self.area}))"
        )
        with self.path(island) as input_fna:
            return trnascan(input_fna)

    def process(self, island: Island):
        """Process the trnascan output to a pandas DataFrame.

        Parameters
        ----------
        island : Island
            The island to annotate.

        Returns
        -------
        pd.DataFrame
            The trnascan output as a pandas DataFrame.
        """
        logger.info(
            f"Processing trnascan output on {island.id} (origin: {self.origin}, area: {self.area}))"
        )
        trnascan_content = self.run(island)
        if not trnascan_content:
            return pd.DataFrame(columns=self._cols)
        df = pd.read_csv(
            StringIO(trnascan_content),
            sep="\t",
            header=None,
            names=self._cols,
        )
        if len(df):
            coord_info = df.cds_name.apply(
                lambda cds_name: pd.Series(
                    _TRNA_NAME_PATTERN.match(cds_name).groupdict()
                )
            ).apply(pd.to_numeric, errors="ignore")
            df["annot_start"] += coord_info["fna_start"]
            df["annot_end"] += coord_info["fna_start"]
            df.insert(
                df.columns.to_list().index("annot_end") + 1,
                "annot_strand",
                (df.annot_start < df.annot_end).map({True: "+", False: "-"}),
            )
            df[["annot_start", "annot_end"]] = df[["annot_start", "annot_end"]].apply(
                sorted, axis=1, raw=True
            )

        return df

    def annotate(self, island: Island):
        """Run the annotator and annotate the island with the results. Island will be
        modified in place.

        Parameters
        ----------
        island : Island
            The island to annotate.

        Returns
        -------
        Island
            The annotated island.
        """
        df = self.process(island)
        for _, row in df.iterrows():
            row = row.to_dict()
            island.add_annotation(
                start=row.pop("annot_start"),
                end=row.pop("annot_end"),
                strand=row.pop("annot_strand"),
                origin=self.origin,
                quals=row,
            )
        return island
