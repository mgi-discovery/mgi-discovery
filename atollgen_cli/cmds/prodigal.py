""" Run prodigal on the nucleotide fasta file to predict and extract protein-coding
genes.

TODO: use pyrodigal instead of prodigal cli (https://github.com/althonos/pyrodigal)
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from __future__ import annotations

from pathlib import Path
from typing import Literal

import plumbum
from loguru import logger
from pydantic import FilePath, validate_arguments

from ..utils import log_cmd, warn_missing

try:
    _PRODIGAL_CMD = plumbum.local["prodigal"]
except plumbum.CommandNotFound:
    raise ImportError(warn_missing("Prodigal cli"))


@validate_arguments
def prodigal(
    fna_input: FilePath,
    faa_output: Path,
    mode: Literal["single", "meta"] = "meta",
) -> str:
    """Run prodigal on the nucleotide fasta file to predict and extract protein-coding
    genes.

    Parameters
    ----------
    fna_input : FilePath
        nucleotide fasta input
    faa_output : Path
        amino acid fasta output
    mode : str, optional
        Specify mode ("single" or "meta"). Defaults to "meta".

    Returns
    -------
    Path
        amino acid fasta output

    Notes
    -----

    The prodigal command is:

        prodigal -q -c -m -p <mode> -a <faa_output> -i <fna_input>
    """

    cmd = _PRODIGAL_CMD["-q", "-c", "-m", "-p", mode, "-a", faa_output, "-i", fna_input]
    logger.info(log_cmd(cmd))
    logger.debug(cmd())
    return faa_output
