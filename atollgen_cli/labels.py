""" Labeling functions for islands."""

from collections import defaultdict
from typing import Sequence

import pandas as pd

from .islands import Island


def label_island(
    island: Island,
    actino_taxons: Sequence[int],
    evalue_threshold: float = 1e-2,
    cov_threshold: float = 0.6,
):
    """ Label an island. The labeling is based on the presence of genes and the taxon of
    the island. Island are considered to a GI at minima.

    Labeling is based on the following rules:

    [TODO] add rules

    Parameters
    ----------
    island : Island
        The island to label.
    actino_taxons : Sequence[int]
        The list of actinobacteria taxons.
    evalue_threshold : float, optional
        The evalue threshold to consider a gene as present, by default 1e-2
    cov_threshold : float, optional
        The coverage threshold to consider a gene as present, by default 0.6

    Returns
    -------
    str
        The label of the island. It can be one of the following:
        - uGI: unclassified GI
        - GI
        - IE
        - IME
        - psiICE
        - ICE
        - AICE
        - IE|AICE
    """
    label = "uGI"

    is_actino = island.tax_id in actino_taxons
    if not island.CDSs.values():
        return label
    cds_df = pd.DataFrame(island.CDSs.values()).set_index(["start", "end"])
    annot_df = cds_df.annotations.explode()
    annot_df = pd.json_normalize(cds_df.annotations.explode()).set_index(annot_df.index)
    if "origin" not in annot_df.columns:
        return label
    df = annot_df[annot_df.origin.isin(["mobility", "integrase"])]
    if df.empty:
        return label
    flag = (df["quals.evalue"] < evalue_threshold) & (
        df["quals.coverage_factor"] > cov_threshold
    )
    df = df[flag].copy()
    if df.empty:
        return label
    df["quals.categs"] = df["quals.categs"].str.split(";")
    df = df.explode("quals.categs")

    count_categ = defaultdict(
        lambda: 0,
        df.reset_index()
        .drop_duplicates(["start", "quals.relquery_start", "quals.categs"])[
            "quals.categs"
        ]
        .value_counts()
        .fillna(0),
    )

    if is_IE := (df.origin == "integrase").any():
        label = "IE"
    if is_DTR := is_IE and (
        count_categ["DTR"]
        or (
            (1 <= count_categ["MPF"] <= 3)
            and (count_categ["INT_tyr"] > count_categ["INT_DTR_like"])
        )
    ):
        label = "IME"
    if is_psiICE := is_DTR and (count_categ["MPF"] >= 3) and (count_categ["T4CP"] > 0):
        label = "psiICE"
    if is_psiICE and (df["quals.query_id"] == "virb4").any():
        label = "ICE"
    if is_IE and count_categ["MPF_DTR_like"] and count_categ["REP"]:
        if island.tax_id is None:
            label = "IE|AICE"
        if is_actino:
            label = "AICE"
    return label
