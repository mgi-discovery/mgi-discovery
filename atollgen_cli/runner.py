"""Classes to run island annotation for one or multiple islands. This is the main API
access for the user. It is used by the CLI.

Examples
--------
>>> from atollgen_cli.runner import IslandRunner
>>> runner = IslandRunner()
>>> island = runner.run(accession, start, end)
>>> island = runner.run_many([(accession, start, end), ...])
"""

from __future__ import annotations

import contextlib
import importlib
import json
from os import getenv
from dataclasses import InitVar, dataclass, field
from pathlib import Path
from typing import Iterable, List, Optional, Set, Tuple, Union

from Bio import SeqIO
import joblib
from loguru import logger
from platformdirs import user_data_dir
import pooch
import tqdm
from entrez_fetcher import GenomeStore

from . import resources
from .annotators import (
    Annotator,
    DFinderAnnotator,
    HmmerAnnotator,
    RGIAnnotator,
    TrnaScanAnnotator,
)
from .cmds.hmmer import hmmpress
from .islands import Island
from .labels import label_island


@contextlib.contextmanager
def _tqdm_joblib(tqdm_object):
    """Context manager to patch joblib to report into tqdm progress bar given as
    argument"""

    class TqdmBatchCompletionCallback(joblib.parallel.BatchCompletionCallBack):
        def __call__(self, *args, **kwargs):
            tqdm_object.update(n=self.batch_size)
            return super().__call__(*args, **kwargs)

    old_batch_callback = joblib.parallel.BatchCompletionCallBack
    joblib.parallel.BatchCompletionCallBack = TqdmBatchCompletionCallback
    try:
        yield tqdm_object
    finally:
        joblib.parallel.BatchCompletionCallBack = old_batch_callback
        tqdm_object.close()


ATOLLGEN_INPUT_BASE_URL = "doi:10.5281/zenodo.7866495"
USER_APPDIR = Path(getenv("ATOLLGEN_FOLDER", user_data_dir("atollgen-cli", "atollgen")))


def _create_pooch_registry(
    path=USER_APPDIR, base_url=ATOLLGEN_INPUT_BASE_URL, extra_registry=None
):
    registry = pooch.create(
        # Folder where the data will be stored. For a sensible default, use the
        # default cache folder for your OS.
        path=path,
        # Base URL of the remote data store. Will call .format on this string
        # to insert the version (see below).
        base_url=base_url,
        version_dev="main",
        # An environment variable that overwrites the path.
        env="ATOLLGEN_DATA_DIR",
    )
    # get filename inside the package
    with importlib.resources.path(resources, "data-registry.txt") as fname:
        registry.load_registry(fname)
    if extra_registry is not None:
        registry.load_registry(extra_registry)
    return registry


class LocalGenomeStore(GenomeStore):
    def __init__(self, fasta: Path, taxid: int):
        self.fasta_path = fasta
        self.fasta_name = self.fasta_path.stem
        self.taxid = taxid
        self.read_only = True

    def load_docsums(self, *args, **kwargs):
        return []

    def load_taxonomy(self, *args, **kwargs):
        return []

    def load_sequences(self, *args, **kwargs):
        return []

    def get_genome(self, _):
        return self.fasta_path.read_text()

    def get_genome_filename(self, *args, **kwargs):
        return self.fasta_path

    def get_genome_record(self, *args, **kwargs):
        return SeqIO.read(self.fasta_path, "fasta")

    def get_taxid(self, _):
        return self.taxid


def init_annotators(db_dir: Path = USER_APPDIR) -> List[Annotator]:
    """Initialize the annotators to use for the island annotation pipeline.

    Parameters
    ----------
    db_folder : Path
        The path to the folder containing the databases.
    categ_mapping : Dict[str, str]
        The mapping between Pfam categories and their description.

    Returns
    -------
    List[Annotator]
        The list of annotators to use.
    """
    categ_mapping = json.loads((db_dir / "categs.json").read_text())
    hmm_folder = db_dir / "hmm"
    mc_folder = db_dir / "macsyfinder"
    hmm_int_annot = HmmerAnnotator(
        hmm_folder / "int.hmm",
        origin="integrase",
        area="exts",
        pfam_mapping=categ_mapping,
    )
    hmm_mob_annot = HmmerAnnotator(
        hmm_folder / "mob.hmm",
        origin="mobility",
        area="island",
        pfam_mapping=categ_mapping,
    )
    hmm_pfam_annot = HmmerAnnotator(
        hmm_folder / "Pfam-A.hmm",
        origin="pfam-A",
        area="envs",
        pfam_mapping=categ_mapping,
    )
    dfinder_annot = DFinderAnnotator(mc_folder, origin="defense_finder")
    rgi_annot = RGIAnnotator(origin="rgi")
    trna_annot = TrnaScanAnnotator(origin="trnascan")
    return [
        hmm_int_annot,
        hmm_mob_annot,
        hmm_pfam_annot,
        dfinder_annot,
        rgi_annot,
        trna_annot,
    ]


@dataclass
class DataHolder:
    """Class to hold the data for the island annotation pipeline.

    Parameters
    ----------
    db_dir : Path
        The path to the folder containing the databases.
    url_info : InputUrl
        The url and md5 of the input data.
    ensure_data : bool
        Whether to ensure the data is downloaded and up to date at class instantiation.

    Attributes
    ----------
    inputs_url : str
        The url of the input data.
    download_dir : Path
        The path to the folder where the input data will be downloaded.
    archive_inputs : Path
        The path to the archive containing the input data.
    hmm_dir : Path
        The path to the folder containing the HMMs.
    categ_mapping : Path
        The path to the file containing the mapping between Pfam categories and their
        description.
    actino_taxids : set[int]
        The set of taxids of actinobacteria.
    inputs_md5_ok : bool
        Whether the md5 of the downloaded input data matches the expected one.

    """

    db_dir: Path = USER_APPDIR
    inputs_url: str = ATOLLGEN_INPUT_BASE_URL
    ensure_data: InitVar[bool] = True
    extra_registry_file: InitVar[Optional[str]] = None
    genome_store: Optional[GenomeStore] = None
    progressbar: bool = True

    def __post_init__(
        self,
        ensure_data: bool,
        extra_registry_file: Optional[str] = None,
    ):
        self.db_dir = Path(self.db_dir)
        self.registry = _create_pooch_registry(
            self.db_dir, self.inputs_url, extra_registry_file
        )
        if self.genome_store is None:
            self.genome_store = GenomeStore(self.db_dir / "genomes")
        if ensure_data:
            self.load()

    @property
    def hmm_files(self) -> Path:
        hfiles = self.registry.fetch(
            "hmm.zip",
            processor=pooch.Unzip(extract_dir=""),
            progressbar=self.progressbar,
        )
        return [hfile for hfile in map(Path, hfiles) if hfile.suffix == "hmm"]

    @property
    def categ_mapping(self) -> Path:
        return json.loads(
            Path(
                self.registry.fetch("categs.json"), progressbar=self.progressbar
            ).read_text()
        )

    @property
    def actino_taxids(self) -> Set[int]:
        taxid_file = Path(
            self.registry.fetch(
                "actinobacteria_taxids.txt", progressbar=self.progressbar
            ),
        )
        return set(map(int, taxid_file.read_text().strip().split("\n")))

    def fetch_defense_finder(self) -> None:
        self.registry.fetch(
            "macsyfinder.zip",
            processor=pooch.Unzip(extract_dir=""),
            progressbar=self.progressbar,
        )

    def hmmpress(self) -> None:
        """Run hmmpress on the HMMs."""
        for hmm_file in self.hmm_files:
            if hmm_file.suffix != ".hmm":
                continue
            print(hmm_file)
            logger.info(f"Running hmmpress on {hmm_file}")
            hmmpress(hmm_file)

    def load(self):
        self.actino_taxids
        self.categ_mapping
        self.hmmpress()
        self.fetch_defense_finder()

    def preload_genomes(self, uids: Union[str, Iterable[str]]):
        uids = set(uids)
        logger.info(f"Preloading {len(uids)} genomes from NCBI")
        logger.info("Loading docsums")
        self.genome_store.load_docsums(uids)
        logger.info("Loading taxonomy")
        self.genome_store.load_taxonomy(uids)
        logger.info("Loading sequences")
        self.genome_store.load_sequences(uids, max_batch_size=10)


class AtollgenRunner:
    def __init__(
        self,
        db_dir=USER_APPDIR,
        annotators_factory=init_annotators,
        ensure_data=False,
        genome_store=None,
    ):
        db_dir = Path(db_dir)
        self.data_holder = DataHolder(
            db_dir=db_dir, ensure_data=ensure_data, genome_store=genome_store
        )
        self.annotators = annotators_factory(db_dir=db_dir)

    def check(self):
        """Check if all the required databases are present."""
        for annotator in self.annotators:
            annotator.check()

    def ensure_data(self):
        """Download the required databases.

        Parameters
        ----------
        force : bool
            Whether to force the download of the data even if they are already
            downloaded.
        """
        try:
            self.check()
        except Exception:
            self.data_holder.load()

    def preload_from_ncbi(self, uids: Union[str, Iterable[str]]):
        """Preload genomes from NCBI. This will download the docsums, taxonomy and
        sequences of the genomes for the given uids (accessions).
        """
        self.data_holder.preload_from_ncbi(uids)

    @property
    def genome_store(self):
        return self.data_holder.genome_store

    def run(
        self,
        accession: str,
        start: int,
        end: int,
        output_file: Optional[Path] = None,
        keep_empty_cds: bool = True,
        tax_id: Optional[int] = None,
    ):
        """Run the annotators on the given island. The island is defined by its
        accession, start and end.

        Parameters
        ----------
        accession : str
            The accession of the genome.
        start : int
            The start of the island.
        end : int
            The end of the island.
        output_file : Path | None
            The file to write the island to as json file. If None, the island is not
            written.
        keep_empty_cds : bool
            Whether to keep the CDSs that do not have a annotations.

        Returns
        -------
        Island
            The annotated and labeled island.
        """
        if tax_id is None:
            tax_id = self.genome_store.get_taxonomy(accession).tax_id
        island = Island(
            accession=accession,
            start=start,
            end=end,
            tax_id=tax_id,
            genome_store=self.genome_store,
        )
        for annot in self.annotators:
            island.annotate(annot)
        island.label = label_island(island, self.data_holder.actino_taxids)

        if output_file is not None:
            Path(output_file).write_text(island.to_json(keep_empty_cds=keep_empty_cds))
        return island

    def _run_robust(self, *args, **kwargs):
        try:
            return self.run(*args, **kwargs)
        except Exception as e:
            logger.exception(e)
            return None

    def run_many(
        self,
        island_coords: Iterable[Tuple[str, int, int]],
        output_dir: Optional[Path] = None,
        keep_empty_cds: bool = True,
        overwrite=False,
    ) -> List[Island]:
        """Run the annotators on the given islands. The islands are defined by their
        accession, start and end.

        Parameters
        ----------
        island_coords : Iterable[Tuple[str, int, int]]
            The coordinates of the islands to annotate. Each coordinate is a tuple of
            the accession, start and end.
        output_dir : Path | None
            The directory where to write the islands as json files. If None, the islands
            are not written.
        keep_empty_cds : bool
            Whether to keep the CDSs that do not have a annotations.

        Returns
        -------
        List[Island]
            The annotated and labeled islands.
        """
        futures = []
        for accession, start, end in island_coords:
            out_file = output_dir / f"{accession}_{start}-{end}.json"
            if not overwrite and out_file.exists():
                continue
            future = joblib.delayed(self._run_robust)(
                accession=accession,
                start=start,
                end=end,
                output_file=output_dir / f"{accession}_{start}-{end}.json"
                if output_dir is not None
                else None,
                keep_empty_cds=keep_empty_cds,
            )
            futures.append(future)
        with _tqdm_joblib(
            tqdm.auto.tqdm(desc="Processing uids...", total=len(futures))
        ):
            return joblib.Parallel()(futures)
