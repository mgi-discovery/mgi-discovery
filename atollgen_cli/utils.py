from loguru import logger
from plumbum.commands.base import BoundCommand


def log_cmd(bound_cmd: BoundCommand) -> str:
    return f"run {bound_cmd.cmd.executable.stem} {' '.join(map(str, bound_cmd.args))}"


def warn_missing(missing):
    return (
        f"Impossible to find {missing}. "
        "Check if it is installed and discoverable by MGI Discovery."
    )
